import pygame
import time
import random

# always have to start with initiation of pygame
pygame.init()

# set up a window where the game will be
# with the width and height provided in tuple
display_width = 800
display_height = 600
game_display = pygame.display.set_mode((display_width, display_height))

# define colors in rgb
black = (0, 0, 0)
white = (255, 255, 255)
red = (200, 55, 55)
bright_red = (255, 0, 0)
bright_green = (0, 255, 0)
light_button = (0, 48, 143)


earth_width = 73
earth_height = 75

# window title
pygame.display.set_caption('Save our home!!!')

# set the game clock as it imposes on every action
clock = pygame.time.Clock()


# upload earth image. In the string we can provide the full path C:\Users...
earth_img = pygame.image.load('earth.png')

# create the file with best scores
with open('bestscore.txt', 'w+') as f:
    f.write(str(0) + '\n')

# button fce..mesage, where it is, width and height, colors
def button(msg, x, y, w, h, inactive_color, active_color, action=None):
    # get the mouse position..it prints the tuple x, y position
    mouse = pygame.mouse.get_pos()

    # get the action from mouse - click
    click = pygame.mouse.get_pressed()
    # display the button to start the game
    # check if user hover with the mouse over the button..then display the button in different color
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(game_display, active_color, (x, y, w, h))
        # mouse click is 1 in the first number in click tupple, second is middle button, third is right button
        if click[0] == 1 and action != None:
            if action == 'play':
                game_loop()
            elif action == 'quit':
                pygame.quit()
                quit()

    # and display in white if the mouse is not on the button
    else:
        pygame.draw.rect(game_display, inactive_color, (x, y, w, h))

    # add the text to the button
    smallText = pygame.font.Font('freesansbold.ttf', 20)
    textSurf, textRec = text_objects(msg, smallText)
    textRec.center = ((x + (w / 2)), (y + (h / 2)))
    game_display.blit(textSurf, textRec)


# fce for the game intro, first screen
def game_intro():

    intro = True

    while intro:
        for event in pygame.event.get():
            # print(event)
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

        game_display.fill(black)
        largeText = pygame.font.Font('freesansbold.ttf', 50)
        TextSurf, TextRec = text_objects('Welcome to my game', largeText)
        TextRec.center = ((display_width / 2), (display_height / 2))
        game_display.blit(TextSurf, TextRec)

        # define a button
        button('Start', 200, 450, 100, 50, black, light_button, 'play')
        button('Quit', 550, 450, 100, 50, black, light_button, 'quit')

        pygame.display.update()
        clock.tick(15)


# to display the score of the game
def things_dodged(count):
    # create my font which will be used to display my score
    font = pygame.font.SysFont(None, 25)
    # and then render text using the font
    text = font.render("Dodged planets: {}".format(count), True, red)
    # every time I want to display anything I needto blit it
    # what do we want to blit and where do we want to blit
    game_display.blit(text, (0, 0))

# to display the best score
def best_score(bestscore):
    font = pygame.font.SysFont(None, 25)
    text = font.render("Your best score: {}".format(bestscore), True, red)
    game_display.blit(text, (640, 0))

# defining the object/enemy planet, x,y - where it is. w - width, h - height and color
def things(thingx, thingy, thingw, thingh, color):
    # builtin fce to draw an object..with color and defining location
    pygame.draw.rect(game_display, color, [thingx, thingy, thingw, thingh])

# check if the thing is out of the screen and start a new one
def start_things(thing_starty, display_height, thing_startx):
    if thing_starty > display_height:
        thing_starty = - 100
        thing_startx = random.randrange(0, display_width)
        thing_start = (thing_starty, thing_startx)
        return thing_start

# check if the earth crashed in the enemy planet
def crash_check(thing_starty,thing_height,y, thing_startx, x, thing_width, earth_width, dodged_creatures):
    if thing_starty + thing_height >= y >= thing_starty:
        # check if the thing/enemy planet has hit the earth....from sides
        # before or we check if the earth is directly hitting the thing/enemy planet
        if thing_startx <= x <= thing_startx + thing_width or thing_startx <= x + earth_width <= thing_startx + thing_width:
            # adding achieved score to best score file
            with open('bestscore.txt', 'a') as f:
                f.write(str(dodged_creatures) + '\n')
            earth_dies()

# display the earth using fce
# display via blit (image and the location on screen)
def earth(x, y):
    game_display.blit(earth_img, (x, y))

def planet(image, x, y):
    game_display.blit(image, (x, y))

# fce to show the text what is used in message_display fce. Showing the text and font
def text_objects(text, font):
    # to render..first is text, second is entelising and last is the color
    textSurface = font.render(text, True, white)
    # and return the render of the tex and the actual rectangle of the the text
    return textSurface, textSurface.get_rect()

# fce to display a message
def message_display(text):
    # message with the font name and Font size
    largeText = pygame.font.Font('freesansbold.ttf', 50)
    # text surface and text rectangle..using another fce to show the text and the font and Font size
    TextSurf, TextRec = text_objects(text, largeText)
    # to center the text what we wanto to show in the middle of the screen
    TextRec.center = ((display_width/2), (display_height/2))
    # message display
    game_display.blit(TextSurf, TextRec)
    # udpate the display
    pygame.display.update()
    # make the game to display the message for certain time
    time.sleep(2)
    # and to start the game again I have to reference the game loop
    game_loop()

# fce to handle what happens after earth dies
def earth_dies():

    largeText = pygame.font.Font('freesansbold.ttf', 50)
    TextSurf, TextRec = text_objects('Everybody died bro', largeText)
    TextRec.center = ((display_width / 2), (display_height / 2))
    game_display.blit(TextSurf, TextRec)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
    # define a button
        button('Play again', 200, 450, 100, 50, black, light_button, 'play')
        button('Quit', 550, 450, 100, 50, black, light_button, 'quit')

        pygame.display.update()
        clock.tick(15)

def game_loop():
    # displays a earth at the bottom of the scree as a start
    x = (display_width * 0.45)
    y = (display_height * 0.8)

    # variable to capture earth movement
    x_change = 0
    y_change = 0

    # list of all planets what they attack the earth
    all_planets = ['jupiter.png', 'mars.png', 'mercury.png', 'neptun.png', 'saturn.png', 'sun.png']

    # 1 enemy planet...to display for the first time
    picked_planet1 = all_planets[random.randint(0, 5)]
    planet1_img = pygame.image.load(picked_planet1)

    # 2 enemy planet...to display for the first time
    picked_planet2 = all_planets[random.randint(0, 5)]
    planet2_img = pygame.image.load(picked_planet2)

    # 3 enemy planet...to display for the first time
    picked_planet3 = all_planets[random.randint(0, 5)]
    planet3_img = pygame.image.load(picked_planet3)

    # count of dodged enemy planet for the score
    dodged_enemy_planets = 0

    # defining 1 enemy planet
    # setting from where enemy planet/things will come from...using random fce
    # so it is starts anywhere at the top of the screen...on the x axis
    thing_startx = random.randrange(0, display_width)
    # enemy planet/things start 600 pixels above the screen
    thing_starty = -600
    # setting how fast they will run down
    # each time we will redraw..it will move 7 pixels
    thing_speed = 7
    # thing/enemy planet width
    thing_width = 100
    # thing/enemy planet height
    thing_height = 100

    # defining 2 enemy planet
    thing2_startx = random.randrange(0, display_width)
    thing2_starty = -600
    thing2_speed = 3
    thing2_width = 100
    thing2_height = 100

    # defining 3 enemy planet
    thing3_startx = -600
    thing3_starty = random.randrange(0, display_height)
    thing3_speed = 7
    thing3_width = 100
    thing3_height = 100

    # setting the game logic..loop, end of the game
    game_exit = False

    while not game_exit:

        # everything what runs in pygame is an event
        for event in pygame.event.get():
        # in case user hits Escape, ends the game
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            # control a earth..check if the event is key movement..if it is pressed..keydown
            if event.type == pygame.KEYDOWN:
            # if its a left arrow
                if event.key == pygame.K_LEFT:
                    x_change = -5
                if event.key == pygame.K_RIGHT:
                    x_change = 5
                if event.key == pygame.K_DOWN:
                    y_change = 5
                if event.key == pygame.K_UP:
                    y_change = -5

            # control a earth..check if the event is key movement..if key is released..keyup
            # for release to update the x_change and y_change..so it is not moving
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0
                if event.key == pygame.K_DOWN or event.key == pygame.K_UP:
                    y_change = 0
        # update the x...the location
        x += x_change
        y += y_change

        # set the background color
        game_display.fill(black)

        # display 1 thing/enemy planet
        # def things(thingx, thingy, thingw, thingh, color)
        things(thing_startx, thing_starty, thing_width, thing_height, black)
        # change the thing_starty so it moves down by certain defined speed
        thing_starty += thing_speed
        # display the enemy planet 1
        planet(planet1_img, thing_startx, thing_starty)


        # display 2 thing/enemy planet
        things(thing2_startx, thing2_starty, thing2_width, thing2_height, black)
        thing2_starty += thing2_speed
        # display the enemy planet 2
        planet(planet2_img, thing2_startx, thing2_starty)

        # display 3 planet enemy
        things(thing3_startx, thing3_starty, thing3_width, thing3_height, black)
        thing3_startx += thing3_speed
        # display the enemy planet 3
        planet(planet3_img, thing3_startx, thing3_starty)

        # display a earth
        earth(x, y)

        # display a score
        things_dodged(dodged_enemy_planets)

        # open a file to get the best score a display it on the screen
        with open('bestscore.txt') as f:
            scores = f.readlines()

        all_scores = []
        for score in scores:
            all_scores.append(int(score.strip()))

        best_score(max(all_scores))

        # 1 enemy planet check if it is out of the screen using fce
        first_enemy_planet = start_things(thing_starty, display_height, thing_startx)
        if type(first_enemy_planet) is tuple:
            # set the new start
            thing_starty = first_enemy_planet[0]
            thing_startx = first_enemy_planet[1]
            # add the score
            dodged_enemy_planets += 1
            # increase the speed
            thing_speed += 0.5
            # change the planet picture
            picked_planet1 = all_planets[random.randint(0, 5)]
            planet1_img = pygame.image.load(picked_planet1)


        # 2 enemy planet check if it is out of the screen using fce
        second_enemy_planet = start_things(thing2_starty, display_height, thing2_startx)
        if type(second_enemy_planet) is tuple:
            thing2_starty = second_enemy_planet[0]
            thing2_startx = second_enemy_planet[1]
            dodged_enemy_planets += 1
            thing2_speed += 1
            picked_planet2 = all_planets[random.randint(0, 5)]
            planet2_img = pygame.image.load(picked_planet2)

        # 3 enemy planet check if it is out of the screen using code
        if thing3_startx > display_width:
            thing3_starty = random.randrange(0, display_height)
            thing3_startx = -600
            dodged_enemy_planets += 1
            thing3_speed += 0.5
            picked_planet3 = all_planets[random.randint(0, 5)]
            planet3_img = pygame.image.load(picked_planet3)

        # 1 enemy planet crash check
        crash_check(thing_starty, thing_height, y, thing_startx, x, thing_width, earth_width, dodged_enemy_planets)
        # check if the thing/enemy planet has hit the earth...it would run over
        # if thing_starty + thing_height >= y >= thing_starty:
            # check if the thing/enemy planet has hit the earth....from sides
            # before or we check if the earth is directly hitting the thing/enemy planet
            # if thing_startx <= x <= thing_startx + thing_width or thing_startx <= x + earth_width <= thing_startx + thing_width:
                # earth_dies()

        # 2 enemy planet crash check
        crash_check(thing2_starty, thing2_height, y, thing2_startx, x, thing2_width, earth_width, dodged_enemy_planets)

        # 3 enemy planet crash check
        if thing3_starty + thing3_height >= y >= thing3_starty:
            if thing3_startx <= x <= thing3_startx + thing3_width or thing3_startx <= x + earth_width <= thing3_startx + thing3_width:
                with open('bestscore.txt', 'a') as f:
                    f.write(str(dodged_enemy_planets) + '\n')
                earth_dies()

        # udpate the display
        pygame.display.update()

        # set frames per second. Increase is that it moves smoothly
        # setting to 60
        clock.tick(40)

game_intro()
game_loop()
pygame.quit()
quit()